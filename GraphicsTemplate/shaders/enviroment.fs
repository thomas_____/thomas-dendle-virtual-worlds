#version 450 core

layout(binding = 0) uniform sampler2D ColTexture;
layout(binding = 2) uniform samplerCube SkyboxTexture;

in float Intensity;
in vec2 TexCoord;

////////////
in vec3 reflectedVector; 
in vec3 reflectFS;
////////////
out vec4 FragColor; // Color that will be used for the fragment

//////////////////////////////////////////////////////////////////
// main()
//////////////////////////////////////////////////////////////////
void main()
{

    //other way

    vec4 reflectedColour = texture(SkyboxTexture, reflectedVector);
    FragColor = mix(FragColor, reflectedColour, 0.6);




    //guys way

    vec4 colour = texture(ColTexture, TexCoord.xy); 
    vec4 reflect = texture(SkyboxTexture, reflectFS);

   // FragColor = vec4(vec3((colour.xyz + reflect.xyz) * Intensity), 1.0);

    //FragColor = vec4(vec3(colour.xyz * Intensity), 1.0);
}