#version 450 core   

layout (location = 0) in vec3               vertexPos_VS;	  	// vp postitions from mesh
layout (location = 1) in vec2               texCoord_VS;        //uneeded
layout (location = 2) in vec3               vertexNormal_VS;    // vn normals from mesh

layout (location = 3) uniform mat4          modelMatrix;   //M 
layout (location = 4) uniform mat4          gVP;           //     //slots, input being addressed
layout (location = 5) uniform vec3          gCP;           // //globalCameraPos, globalViewProjection

layout (location = 6) uniform vec3          lightPosition;

layout (location = 7) uniform mat4          V;
layout (location = 8) uniform mat4          P;      //maybe view and projection matrix, if it works
uniform mat4 viewMatrix;

 out vec3 pos_eye;
 out vec3 n_eye;

//////////////////////////////////////////////////////////////////
// main()
//////////////////////////////////////////////////////////////////
void main()
{    

    //use new vars, simpler.
    pos_eye = vec3(V * modelMatrix * vec4 (vertexPos_VS;, 1.0));
    n_eye = vec3(V * modelMatrix * vec4 (vertexNormal_VS, 0.0));
    gl_Position = P * V * modelMatrix * vec4 (vertexPos_VS;, 1.0);


    //if no work try cutting out gCP
    /*
	//vec3 mpos   = (modelMatrix * vec4(vertexPos_VS, 1.0)).xyz;
    //TexCoord    = texCoord_VS;
    //vec3 Normal 	   = (modelMatrix * vec4(vertexNormal_VS, 0.0)).xyz;     
    //vec3 N = normalize(Normal);
    //vec3 lightDir  = lightPosition - mpos;
    //vec3 L = normalize(lightDir);
    //Intensity = dot(N, L);
    //gl_Position = gVP * vec4(mpos, 1.0);    //position = global view projection * 
    */

    //shiny
   // vec4 worldPosition = 
   // vec3 viewVector = worldPosition.xyz    //calculate the vector from cam to vertex, subtract cam position from world position
    //inputVertexFS.reflectFS = normalise(reflect(V,N));

   
}




//vertex shader for is the one that will be calculating reflections