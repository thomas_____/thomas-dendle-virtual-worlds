#version 450 core

layout(binding = 0) uniform sampler2D ColTexture;

in float Intensity;
in vec2 TexCoord;

out vec4 FragColor; // Color that will be used for the fragment

//////////////////////////////////////////////////////////////////
// main()
//////////////////////////////////////////////////////////////////
void main()
{
    vec4 colour = texture(ColTexture, TexCoord.xy); 


float i = Intensity;

if (Intensity > 0.5)
i = 1.0;
else i = 0.0;

    FragColor = vec4(vec3(colour.xyz * i * vec3(1.0, 1.0, 0.1)), 1.0);
}