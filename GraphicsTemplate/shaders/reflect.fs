#version 450 core

//in float Intensity;
//in vec2 TexCoord;


in vec3 pos_eye;
in vec3 n_eye;

uniform samplerCube cube_texture;
uniform mat4 gVP; //view matrix V
layout (location = 7) uniform mat4          V;

out vec4 FragColor; // Color that will be used for the fragment

/////////reflections
//in vec3 Normal;
//in vec3 Position;

//uniform vec3 cameraPos;
//uniform samplerCube skybox;


//////////////////////////////////////////////////////////////////
// main()
//////////////////////////////////////////////////////////////////
void main()
{
//reflect ray around normal form eye to surface

vec3 incident_eye = normalize (pos_eye);
vec3 normal = normalize (n_eye);

vec3 reflected = reflect (incident_eye, normal);    //normalise both values 
//convert from eye to world space

reflected = vec3 (inverse (gVP) * vec4 (refleceted, 0.0));

FragColor = texture (cube_texture, reflected);

//maybe delete v
//vec4 colour = texture(ColTexture, TexCoord.xy); 

    //reflections

    //vec3 I = normalise(Position - cameraPos);
   // vec3 R = reflect(I, normalise(Normal));

   // FragColor = vec4(texture(skybox, R).rgb, 1.0);  

    //FragColor = vec4(vec3(colour.xyz * Intensity), 1.0);
}