#version 450 core

layout(binding = 0) uniform sampler2D ColTexture;
layout(binding = 1) uniform sampler2D ColTexture2;


in float Intensity;
in vec2 TexCoord;

out vec4 FragColor; // Color that will be used for the fragment

//////////////////////////////////////////////////////////////////
// main()
//////////////////////////////////////////////////////////////////
void main()
{


    vec4 colour1 = texture(ColTexture2, TexCoord.xy);    //picking a piexl out of the texture. repeated for every pixel
    vec4 colour2 = texture(ColTexture, TexCoord.xy);

    //vec4 colour2 = texture(ColTexture2, TexCoord.xy);  
    vec4 colour = (colour1 * 0.5) + (colour2);   //take colour variables from each texture and multiply them by a number to make it change colour.  
                                                    

     //TEXTURE BLENDING ^^^^ FIND OUT HOW TO DO TEXTURE STENCILS



    FragColor = vec4(vec3(colour.xyz * Intensity), 1.0);    //colour is colour and intensity
}