#version 450 core

layout (location = 0) in vec3               vertexPos_VS;	  	// Position in attribute location 0
layout (location = 1) in vec2               texCoord_VS;
layout (location = 2) in vec3               vertexNormal_VS;

layout (location = 3) uniform mat4          modelMatrix;
layout (location = 4) uniform mat4          gVP;            //slots, input being addressed

layout (location = 5) uniform vec3          gCP;            //globalCameraPos, globalViewProjection
layout (location = 6) uniform vec3          lightPosition;


/////
out vec3 reflectFS;
out vec3 reflectedVector;
////

out float Intensity;
out vec2 TexCoord;


//////////////////////////////////////////////////////////////////
// main()
//////////////////////////////////////////////////////////////////
void main()
{    
	vec3 mpos   = (modelMatrix * vec4(vertexPos_VS, 1.0)).xyz;

    TexCoord    = texCoord_VS;
    
    vec3 Normal 	   = (modelMatrix * vec4(vertexNormal_VS, 0.0)).xyz;     
    vec3 N = normalize(Normal);

    vec3 lightDir  = lightPosition - mpos;
    vec3 L = normalize(lightDir);

    Intensity = dot(N, L);

    ////////////////
    vec3 V = gCP - mpos.xyz;
    vec3 viewVector = normalize(mpos.xyz - gCP);

    reflectedVector = reflect(viewVector, N);
    
    //or

    reflectFS = normalize(reflect(V,N));

    //grabbing a viewvector to cross that and normal to get a reflection vector. 
    ///////////////

    gl_Position = gVP * vec4(mpos, 1.0);    //position = global view projection * 
}