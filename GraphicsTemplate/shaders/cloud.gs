////geometry shader
#version 450


//VARIABLES
uniform mat4 mvpMatrix; 

///INPUT

layout (triangles) in;	//position in attribute location 0

///OUTPUT 

layout (line_strip, max_vertices = 3) out;
//input triangles, output points. 3 vertices at a time

//////////////////////////

/*
in GSPacket {		//packets are structs, output it to an array of vars. 

	vec4colour;
} inputVertex[];		//input data arrives as an array

out SimplePacket {

 vec4 colour	//GS output goes to rasteriser and then fragment shader

} gs_out;
*/

////////////MAIN

void main(void) {

gl_Position = gl_in[0].gl_Position; //each output vertex is built in turn, the gl.Position output from the VS is picked up by gl_in
EmitVertex();	//emits the current vertex 

gl_Position = gl_in[1].gl_Position;
EmitVertex();
EndPrimitive();	//ends drawing the current "shape"

gl_Position = gl_in[1].gl_Position;
EmitVertex();

gl_Position = gl_in[2].gl_Position;
EmitVertex();
EndPrimitive();

gl_Position = gl_in[2].gl_Position;
EmitVertex();

gl_Position = gl_in[0].gl_Position;
EmitVertex();
EndPrimitive();

//start at one point, end at the same point. 


//draw one vertex, then another, then draw a line between them.

}