#include "cSkybox.h"
#include "core.h"



//////////////////////////////////////////////////////////////////////////////////////////
// cSkybox() - constructor
//////////////////////////////////////////////////////////////////////////////////////////
cSkybox::cSkybox()
{
	m_tex = NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////
// ~cSkybox() - destructor
//////////////////////////////////////////////////////////////////////////////////////////
cSkybox::~cSkybox()
{
	if (m_tex)
	{
		for (int i = 0; i < 6; i++)
		{
			delete[] m_tex[i];
			m_tex[i] = NULL;
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////
// init() - 
//////////////////////////////////////////////////////////////////////////////////////////
void cSkybox::init()
{
	for (int i = 0; i < 6; i++) {

		cTex.loadCubeMapTGA(m_tex[i], i);
	}

	
	//char** m_tex;
	//loadCubeMapTGA(char* , int 1);
	//loadCubeMapTGA(char* TexName, int face);
	//cTex.loadCubeMapTGA(m_Tex[i],i)
	// load in all textures for this environment map here.
}


void cSkybox::dynamic() {

	//update(pScene);


}

////UPDATE	

void cSkybox::update(cScene* pScene) 

{

	vec3 map_directions[6] = {
	vec3(-1.0, 0.0, 0.0),		//left
	vec3(1.0, 0.0, 0.0),		//right
	vec3(0.0, -1.0, 0.0),		//forward
	vec3(0.0, 1.0, 0.0), 	//back
	vec3(0.0, 0.0, -1.0),		//up	
	vec3(0.0, 0.0, 1.0)			//down

	};


	//a pointer to the camera
		cCamera* pCamera = &pScene->m_Camera[pScene->m_cameraId];

		//store pos and rot so the cam can be put back later. 
		vec3 camera_start_pos = pCamera->m_pos;
		float camera_start_fov = pCamera->m_fov;
	
		//move camera to the position of of the player mesh. 
		pCamera->m_pos = pScene->m_Mesh[5].m_transform.m_pos;

		//setting FOV
		pCamera->m_fov = 90.0f;
		pCamera->init();

		//LOOP FOR EACH FACE, calculating cube map
		for (int i = 0; i < 6; i++)
		{
			pCamera->LookAt(map_directions[i]);			//for each loop, look at the right scene and then render. 

			//Rendering the scene
			pScene->renderWithoutPlayer();	//do not render the first object

			//Grabbing a copy of what has just been rendered from the BACK buffer
			glReadBuffer(GL_BACK);

			//and then convert it into a texture		
			glCopyTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, 0, 0, 1024.0f, 1024.0f, 0);	//no x or y, same size as screen facing face

			//continue drawing to back buffer. 
			glDrawBuffer(GL_BACK);	
			  
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

		}
		//after the loop, put camera back to oringal position
		pCamera->m_pos = camera_start_pos;
		pCamera->m_fov = camera_start_fov;
		
		pCamera->init();
		
	}		//end of update			
	
	